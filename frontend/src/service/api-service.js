
//const TOKEN = "3ddb1cc254e29784d5d459ee1427e025df122995";

export class API {


        static RegisterUser( body){
          return fetch(`http://127.0.0.1:8000/api/users/`, {
          method: 'POST',
          headers: {
                  'Content-Type': 'application/json',
          },
          body: JSON.stringify( body )
          })
          .then(resp => resp.json())
          .catch(error => console.log(error))

        }


        static LoginUser( body){
          return fetch(`http://127.0.0.1:8000/auth/`, {
          method: 'POST',
          headers: {
                  'Content-Type': 'application/json',
          },
          body: JSON.stringify( body )
          })
          .then(resp => resp.json())
          .catch(error => console.log(error))

        }


	static updateMovie(movie_id, body,token){
          return fetch(`http://127.0.0.1:8000/api/movies/${movie_id}/`, {
          method: 'PUT',
          headers: {
                  'Content-Type': 'application/json',
                  'Authorization': `Token ${token}`,
          },
          body: JSON.stringify( body )
          })
	  .then(resp => resp.json())
	  .catch(error => console.log(error))

	}

        static createMovie( body,token ){
          return fetch(`http://127.0.0.1:8000/api/movies/`, {
          method: 'POST',
          headers: {
                  'Content-Type': 'application/json',
                  'Authorization': `Token ${token}`,
          },
          body: JSON.stringify( body )
          })
          .then(resp => resp.json())
          .catch(error => console.log(error))

        }

        static deleteMovie( movie_id,token){
          return fetch(`http://127.0.0.1:8000/api/movies/${movie_id}/`, {
          method: 'DELETE',
          headers: {
                  'Content-Type': 'application/json',
                  'Authorization': `Token ${token}`,
          },
          })
          .catch(error => console.log(error))

        }

}
