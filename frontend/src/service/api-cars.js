export class APICARS { 


	  static allcars( token ){
          return fetch(`http://127.0.0.1:8000/cars/cardetails/`, {
          method: 'GET',
          headers: {
                  'Content-Type': 'application/json',
                  'Authorization': `Token ${token}`,
          },
          })
          .then(resp => resp.json())
          .catch(error => console.log(error))
        }


        static importCars( body,token ){
          return fetch(`http://127.0.0.1:8000/cars/cardetails/import_cars/`, {
          method: 'POST',
          headers: {
                  'Content-Type': 'application/json',
                  'Authorization': `Token ${token}`,
          },
          body: JSON.stringify( body ) 
          })
          .then(resp => resp.json())
          .catch(error => console.log(error))
	}

}
