import React, { useState,  useEffect } from 'react';
import { API } from '../service/api-service';


import { useCookies } from 'react-cookie';

function Auth(){

        const [username,setUsername] = useState('');
	const [password,setPassword] = useState('');
//	const {token, setToken} = useContext(TokenContext);
        const [token, setToken] = useCookies(['mr-token']);
        const [userId, setUserId] = useCookies(['userId']);
        const [isLoginView,setIsLoginView] = useState(true);

	const loginClicked = () => {
		console.log("login Clicked ");
                API.LoginUser({username:username,password:password})
		  .then(resp => setToken('mr-token', resp.token))
		  .then(resp => setUserId('userId', username))
		  .catch(error => console.log(error))
	}

	const registerClicked = () => {
                console.log("register Clicked ");
                API.RegisterUser({username:username,password:password})
                  .then(() => loginClicked())
                  .catch(error => console.log(error))

	}

        useEffect( () =>{
		console.log(token);
		if(token['mr-token'] ) window.location.href = '/dashBoard';
		 
	},[token])

	return (
                <div>
		{isLoginView ? <h1>Sign In</h1>  : <h1>Register</h1>}
                <label htmlFor="username">Username</label><br/>
                <input id="username" type="text" placeholder="Username" value={username}
                        onChange={evt => setUsername(evt.target.value)}
                        /><br/>
                <label htmlFor="password">password</label><br/>
                <input id="password" type="password" placeholder="password" value={password}
                        onChange={evt => setPassword(evt.target.value)} /><br/>
		{ isLoginView ?
			<button onClick={loginClicked}>Login</button>:
			<button onClick={registerClicked}>Register</button>
		}
		{ isLoginView ?
           		<p onClick={() => setIsLoginView(false)}> You don't have an account ? Register here </p> :
		<p onClick={() => setIsLoginView(true)}> You have an account ? Login  here </p>
		}
                </div>

	)
}

export default Auth;
