import React, { useEffect, useState } from 'react';
import { useCookies } from 'react-cookie';
import {OutTable, ExcelRenderer} from 'react-excel-renderer';
import { APICARS } from '../service/api-cars';
import SimpleTable from './SimpleTable';

function DashBoard(props){
	 const [ token,setToken,removeToken ] = useCookies(['mr-token']);
	 const [ userId ] = useCookies(['userId']);
	 const [cols,setCols] = useState(null);
	 const [rows,setRows] = useState(null);
	 const [cars,setCars] = useState(null);
	 const logoutUser = () => {
        	  removeToken(['mr-token']);
 		 }

	 useEffect(()=>{
		  const allcars =  APICARS.allcars(token['mr-token']);
		 setCars(allcars);
	  }, [])


	 useEffect( () =>{
           console.log(token);
           if(!token['mr-token']) window.location.href = '/';
            },[token])


	 const fileHandler = (event) => {
		 let fileObj = event.target.files[0];
                 //just pass the fileObj as parameter
                 ExcelRenderer(fileObj, (err, resp) => {
                 if(err){
                      console.log(err);
                 }
                 else{
			 setCols(resp.cols);
			 setRows(resp.rows);
                         console.log(resp.cols);
                         console.log(resp.rows);
			 console.log(token['mr-token']);
			 APICARS.importCars(resp.rows,token['mr-token']);
                     }
             });
        }



	return(
	<div>
              <header className="App-header">
       		  <h4>MovieRater</h4>
       		   FontAwesomeIcon icon faSignOutAlt  onClick logoutUser 
   	        </header>

		<div>
		<h1>I am in dahshboard {userId['userId']} </h1>
		<input type="file" onChange={fileHandler} style={{"padding":"10px"}} />
		</div>

		<div>
		<SimpleTable />
		</div>
	</div>
	)
}
export default DashBoard;
