from django.urls import path
from rest_framework import routers
from django.conf.urls import include
from .views import CarViewSet

router = routers.DefaultRouter()
router.register('cardetails', CarViewSet)
#router.register('ratings', RatingViewSet)
#router.register('users',UserViewSet)

urlpatterns = [
        path('',include(router.urls)),

]

