from django.shortcuts import render
from rest_framework import viewsets, status
from .models import Car
from .serializers import CarSerializer
from rest_framework.permissions import IsAuthenticated, AllowAny
from rest_framework.authentication import TokenAuthentication
from rest_framework.decorators import action


class CarViewSet(viewsets.ModelViewSet):
    queryset = Car.objects.all()
    serializer_class = CarSerializer
    authentication_classes = (TokenAuthentication, )
   # permission_classes = (IsAuthenticated,)
    permission_classes = (AllowAny,)

    @action(detail=False,methods=['GET','POST'])
    def import_cars(self,request):

        cars = request.data
        col  = cars[0]
        for cardetail in cars[1:]:
            Car.objects.create(car_no=cardetail[0],owner=cardetail[1],address=cardetail[2],chess_no=cardetail[3])
        return Response(response, status=status.HTTP_200_OK)

#            print("car",car)
       # print("cars==",cars)


'''
    @action(detail=True, methods=['POST'])
    def rate_movie(self, request, pk=None):
        if 'stars' in request.data:
            print("PK==",pk)
            movie = Movie.objects.get(id=pk)
            print("movie title",movie.title)
            print("movie description", movie.description)

            stars = request.data['stars']
            user = request.user
            print('user==',user)
            #user_id = request.data['user']
            #user = User.objects.get(id=user_id)

            print("User ==",user)
            try:
                rating = Rating.objects.get(user=user.id, movie=movie.id)
                rating.stars = stars
                rating.save()

                serializer = RatingSerializer(rating, many=False)

                response = {'message':"Rating is updated ", 'result': serializer.data}

                return Response(response, status=status.HTTP_200_OK)
'''
